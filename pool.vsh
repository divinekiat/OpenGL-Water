#version 330

in layout(location = 0) vec3 pos;
in layout(location = 1) vec2 texCoord;

uniform mat4 vpMatrix;
uniform vec4 plane;

out vec2 o_texCoord;

void main() {

	vec4 worldPos = vec4(pos, 1.0);

	gl_Position = vpMatrix * worldPos;

	gl_ClipDistance[0] = dot(worldPos, plane);

	o_texCoord = texCoord;
}