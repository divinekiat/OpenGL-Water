#version 330

in vec2 o_texCoord;

uniform sampler2D tex;
uniform sampler2D causticsAtlas;
uniform vec2 causticsOffset;

out vec4 dest_color;

void main() {

	vec2 causticsTexCoord = o_texCoord;
	causticsTexCoord.x = causticsTexCoord.x * 0.25 + causticsOffset.x * 0.25;
	causticsTexCoord.y = causticsTexCoord.y * 0.25 + 0.75 + causticsOffset.y * 0.25;

	vec4 poolColor = mix(texture(tex, o_texCoord), vec4(0.0, 0.75, 1.0, 1.0), 0.5);

	dest_color = texture(causticsAtlas, causticsTexCoord);
	dest_color = poolColor + (texture(causticsAtlas, causticsTexCoord));

	// dest_color = vec4(1.0f);
}