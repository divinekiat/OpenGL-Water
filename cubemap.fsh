#version 330
in vec3 o_pos;

uniform mat4 vpMatrix;
uniform samplerCube cubeTex;

out vec4 dest_color;

void main() {
	dest_color = vec4(texture(cubeTex, 
							  mat3(vpMatrix)
							  * vec3(o_pos.xy, 1.0)).rgb, 1.0);
}