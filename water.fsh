#version 330
in vec4 o_clipSpace;
in vec2 o_texCoord; 
in vec3 o_toCameraVector;
in vec3 o_fromLightVector;

uniform sampler2D reflectTex;
uniform sampler2D refractTex;
uniform sampler2D dudvMap;
uniform sampler2D normalMap;
uniform sampler2D depthMap;

uniform vec3 lightColor;
uniform float moveFactor;

out vec4 dest_color;

void main() {
	vec2 ndc = (o_clipSpace.xy/o_clipSpace.w)/2.0 + 0.5;
	vec2 reflectTexCoords = vec2(ndc.x, 1-ndc.y); // -ndc.y because reflection is inverted
	vec2 refractTexCoords = ndc;

	float near = 0.1f;
	float far = 100.f;
	float depth = texture(depthMap, refractTexCoords).r;
	float floorDistance = 2.0 * near * far / (far + near - (2*depth - 1) * (far - near));

	depth = gl_FragCoord.z;
	float waterDistance = 2.0 * near * far / (far + near - (2*depth - 1) * (far - near));
	float waterDepth = floorDistance - waterDistance; 

	float waveStrength = 0.1;
	
	vec2 distortedTexCoords = texture(dudvMap, vec2(o_texCoord.x + moveFactor, o_texCoord.y)).rg * waveStrength;
	distortedTexCoords = o_texCoord + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
	vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg * 2.0 - 1.0) * waveStrength * clamp(waterDepth / 20.0, 0.0, 1.0);

	// remove dudv effect
	// totalDistortion = vec2(0.f);

	reflectTexCoords += totalDistortion;
	// reflectTexCoords.x = clamp(reflectTexCoords.x, 0.001, 0.999);
	// reflectTexCoords.y = clamp(reflectTexCoords.y, -0.999, -0.001);

	refractTexCoords += totalDistortion;
	// refractTexCoords = clamp(refractTexCoords, 0.001, 0.999);

	vec4 reflectColor = texture(reflectTex, reflectTexCoords);
	vec4 refractColor = texture(refractTex, refractTexCoords);

	vec4 normalMapColor = texture(normalMap, distortedTexCoords);

	// remove the dudv effect
	// normalMapColor = texture(normalMap, o_texCoord);

	vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0,
					   normalMapColor.b * 3.0,
					   normalMapColor.g * 2.0 - 1.0);
	normal = normalize(normal);

	// freshnel calculation
	vec3 viewVector = normalize(o_toCameraVector);
	float refractiveFactor = dot(viewVector, vec3(normal));
	refractiveFactor = pow(refractiveFactor, 1.33);

	float shininess = 20.0;
	float reflectivity = 0.6;

	vec3 reflectedLight = reflect(normalize(o_fromLightVector), normal);
	float specular = max(dot(reflectedLight, viewVector), 0.0);
	specular = pow(specular, shininess);
	vec3 specularHighlights = lightColor * specular * reflectivity * clamp(waterDepth / 5.0, 0.0, 1.0);

	dest_color = mix(reflectColor, refractColor, refractiveFactor) + vec4(specularHighlights, 0.0);

	dest_color.a = clamp(waterDepth / 5.0, 0.0, 1.0);
} 