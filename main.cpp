#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <numeric>
#include "glutil.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

#define WIDTH 800.f
#define HEIGHT 800.f

using namespace std;

void loadTexture(const char *path, GLuint target) {
	int width, height, n = 1;
	auto data = stbi_load(path, &width, &height, &n, 4);  
	std::cout << "loaded data size: " << width << ", n: " << n << std::endl;
	
	glTexImage2D(target, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	stbi_image_free(data); 
}

float mapNumber(float number, float inMin, float inMax, float outMin, float outMax) {
	return ((number - inMin) / (inMax - inMin)) * (outMax - outMin) + outMin;
}

int main() { 
	if ( !glfwInit() ) {
		cerr << "Cannot initialize glfw.";
		return -1;
	}
	
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	auto window = glfwCreateWindow(WIDTH, HEIGHT, "Fress Wate", NULL, NULL);
	
	if ( !window ) {
		cerr << "Cannot create window.";
		return -1;
	}
	
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	//================================TEXTURES==================================
	
	glEnable(GL_TEXTURE_2D);

	GLuint diffuseMap;
	{
		glGenTextures(1, &diffuseMap);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, diffuseMap);
		loadTexture("Water/textures/tiles.jpg", GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	//===========================================================================

	GLuint reflectionFbo;
	{
		glGenFramebuffers(1, &reflectionFbo);
		glBindFramebuffer(GL_FRAMEBUFFER, reflectionFbo);
	}

	GLuint reflectionTex;
	{
		glGenTextures(1, &reflectionTex);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, reflectionTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, reflectionTex, 0);
	}

	// GLuint reflectionDbo;
	// {
	// 	glGenRenderbuffers(1, &reflectionDbo);
	// 	glBindRenderbuffer(GL_RENDERBUFFER, reflectionDbo);
	// 	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WIDTH, HEIGHT);
	// 	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, reflectionDbo);
	// }

	GLuint refractionFbo;
	{
		glGenFramebuffers(1, &refractionFbo);
		glBindFramebuffer(GL_FRAMEBUFFER, refractionFbo);
	}

	GLuint refractionTex;
	{
		glGenTextures(1, &refractionTex);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, refractionTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, refractionTex, 0);
	}


	GLuint refractionDTex;
	{
		glGenTextures(1, &refractionDTex);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, refractionDTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, WIDTH, HEIGHT, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, refractionDTex, 0);
	}

	GLuint dudvMap;
	{
		glGenTextures(1, &dudvMap);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, dudvMap);
		loadTexture("Water/textures/water_dudv.png", GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	GLuint normalMap;
	{
		glGenTextures(1, &normalMap);
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, normalMap);
		loadTexture("Water/textures/water_normal.png", GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	// this is a 4x4 atlas
	GLuint causticsTex;
	{
		glGenTextures(1, &causticsTex);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, causticsTex);
		loadTexture("Water/textures/caustics_atlas.png", GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	GLuint cubeTex;
	{
		glGenTextures(1, &cubeTex);
		glActiveTexture(GL_TEXTURE8);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);

		loadTexture("Water/textures/px.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
		loadTexture("Water/textures/nx.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
		loadTexture("Water/textures/py.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
		loadTexture("Water/textures/ny.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
		loadTexture("Water/textures/pz.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
		loadTexture("Water/textures/nz.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
		
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	//================================CUBE MAP==================================

	string vshCubemap = "Water/cubemap.vsh";
	string fshCubemap = "Water/cubemap.fsh";

	auto cubemapProgram = loadProgram(vshCubemap, fshCubemap);

	auto u_c_cubeTex = glGetUniformLocation(cubemapProgram, "cubeTex");
	auto u_c_vpMatrix = glGetUniformLocation(cubemapProgram, "vpMatrix");

	glUseProgram(cubemapProgram);

	glUniform1i(u_c_cubeTex, 8);

	glEnable(GL_TEXTURE_CUBE_MAP);

	//===========================================================================

	GLuint vboCubemap;
	glGenBuffers(1, &vboCubemap);
	glBindBuffer(GL_ARRAY_BUFFER, vboCubemap);

	GLfloat Cubemap[] = {
		-1, 1, 0,
		-1, -1, 0,
		1, 1, 0,
		1, -1, 0
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(Cubemap), Cubemap, GL_STATIC_DRAW);

	GLuint vaoCubemap = 0;
	glGenVertexArrays(1, &vaoCubemap);
	glBindVertexArray(vaoCubemap);
	glBindBuffer(GL_ARRAY_BUFFER, vboCubemap);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, 0);

	//====================================POOL===================================

	string vshPool = "Water/pool.vsh";
	string fshPool = "Water/pool.fsh";

	auto poolProgram = loadProgram(vshPool, fshPool);

	// Uniforms naming convention: u = uniform, p = first letter of program
	auto u_p_tex = glGetUniformLocation(poolProgram, "tex");
	auto u_p_vpMatrix = glGetUniformLocation(poolProgram, "vpMatrix");
	auto u_p_plane = glGetUniformLocation(poolProgram, "plane");

	glUseProgram(poolProgram);

	glUniform1i(u_p_tex, 0);

	auto clipPlane = glm::vec4(0.f, -1.f, 0.f, 0.f);

	//============================================================================

	GLuint vboPool;
	glGenBuffers(1, &vboPool);
	glBindBuffer(GL_ARRAY_BUFFER, vboPool);

	auto plane = genPlane(glm::vec3{1.5, 0.0, 0.0}, 
						  glm::vec3{0.0, 0.5, 0.0}, 
						  glm::vec3{-0.75, -0.5, -0.75}, 
						  1);

	auto plane2 = genPlane(glm::vec3{0.0, 0.0, 1.5}, 
						  glm::vec3{0.0, 0.5, 0.0}, 
						  glm::vec3{0.75, -0.5, -0.75}, 
						  1);

	plane.insert(plane.end(), plane2.begin(), plane2.end());

	plane2 = genPlane(glm::vec3{-1.5, 0.0, 0.0}, 
					  glm::vec3{0.0, 0.5, 0.0}, 
					  glm::vec3{0.75, -0.5, 0.75}, 
					  1);

	plane.insert(plane.end(), plane2.begin(), plane2.end());

	plane2 = genPlane(glm::vec3{0.0, 0.0, -1.5}, 
					  glm::vec3{0.0, 0.5, 0.0}, 
					  glm::vec3{-0.75, -0.5, 0.75}, 
					  1);

	plane.insert(plane.end(), plane2.begin(), plane2.end());

	plane2 = genPlane(glm::vec3{1.5, 0.0, 0.0}, 
					  glm::vec3{0.0, 0.0, -1.5}, 
					  glm::vec3{-0.75, -0.5, 0.75}, 
					  1);
	
	plane.insert(plane.end(), plane2.begin(), plane2.end());


	vector<VPosTexNormalTangent> pool(plane.size());

	for(size_t i = 0; i < plane.size()/5; i++) {
		pool[i].pos = plane[i].pos;
		pool[i].normal = plane[i].normal;
		pool[i].tex.x = pool[i].pos.x/1.5 + 0.5;
		pool[i].tex.y = pool[i].pos.y/1.5 + 0.5;
	}

	for(size_t i = plane.size()/5; i < plane.size()*2/5; i++) {
		pool[i].pos = plane[i].pos;
		pool[i].normal = plane[i].normal;
		pool[i].tex.x = pool[i].pos.z/1.5 + 0.5;
		pool[i].tex.y = pool[i].pos.y/1.5 + 0.5;
	}

	for(size_t i = plane.size()*2/5; i < plane.size()*3/5; i++) {
		pool[i].pos = plane[i].pos;
		pool[i].normal = plane[i].normal;
		pool[i].tex.x = pool[i].pos.x/1.5 + 0.5;
		pool[i].tex.y = pool[i].pos.y/1.5 + 0.5;
	}

	for(size_t i = plane.size()*3/5; i < plane.size()*4/5; i++) {
		pool[i].pos = plane[i].pos;
		pool[i].normal = plane[i].normal;
		pool[i].tex.x = pool[i].pos.z/1.5 + 0.5;
		pool[i].tex.y = pool[i].pos.y/1.5 + 0.5;
	}

	for(size_t i = plane.size()*4/5; i < plane.size(); i++) {
		pool[i].pos = plane[i].pos;
		pool[i].normal = plane[i].normal;
		pool[i].tex.x = pool[i].pos.x/1.5 + 0.5;
		pool[i].tex.y = pool[i].pos.z/1.5 + 0.5;
	}

	glBufferData(GL_ARRAY_BUFFER, sizeof(VPosTexNormalTangent) * pool.size(), pool.data(), GL_STATIC_DRAW);

	GLuint vaoPool = 0;
	glGenVertexArrays(1, &vaoPool);
	glBindVertexArray(vaoPool);
	glBindBuffer(GL_ARRAY_BUFFER, vboPool);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), (void*)(offsetof(VPosTexNormalTangent, tex)));

	//==================================FLOOR===================================

	string vshFloor = "Water/floor.vsh";
	string fshFloor = "Water/floor.fsh";

	auto floorProgram = loadProgram(vshFloor, fshFloor);

	auto u_f_tex = glGetUniformLocation(floorProgram, "tex");
	auto u_f_vpMatrix = glGetUniformLocation(floorProgram, "vpMatrix");
	auto u_f_plane = glGetUniformLocation(floorProgram, "plane");
	auto u_f_causticsAtlas = glGetUniformLocation(floorProgram, "causticsAtlas");
	auto u_f_causticsOffset = glGetUniformLocation(floorProgram, "causticsOffset");

	glUseProgram(floorProgram);

	glUniform1i(u_f_tex, 0);
	glUniform1i(u_f_causticsAtlas, 7);

	auto causticsOffset = glm::vec2{0.0, 0.0};

	glUniform2fv(u_f_causticsOffset, 1, glm::value_ptr(causticsOffset));

	//============================================================================

	GLuint vboFloor;
	glGenBuffers(1, &vboFloor);
	glBindBuffer(GL_ARRAY_BUFFER, vboFloor);

	auto floorPlane = genPlane(glm::vec3{1.5, 0.0, 0.0}, 
							   glm::vec3{0.0, 0.0, -1.5}, 
							   glm::vec3{-0.75, -0.49999, 0.75}, 
							   64);

	vector<VPosTexNormalTangent> floor2(floorPlane.size());

	for(size_t i = 0; i < floorPlane.size(); i++) {
		floor2[i].pos = floorPlane[i].pos;
		floor2[i].normal = floorPlane[i].normal;
		floor2[i].tex.x = mapNumber(floor2[i].pos.x, -0.75, 0.75, 0, 1);
		floor2[i].tex.y = mapNumber(floor2[i].pos.z, 0.75, -0.75, 0, 1);
	}

	glBufferData(GL_ARRAY_BUFFER, sizeof(VPosTexNormalTangent) * floor2.size(), floor2.data(), GL_STATIC_DRAW);

	GLuint vaoFloor = 0;
	glGenVertexArrays(1, &vaoFloor);
	glBindVertexArray(vaoFloor);
	glBindBuffer(GL_ARRAY_BUFFER, vboFloor);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), (void*)(offsetof(VPosTexNormalTangent, tex)));

	//==================================WATER====================================

	string vshWater = "Water/water.vsh";
	string fshWater = "Water/water.fsh";

	auto waterProgram = loadProgram(vshWater, fshWater);

	auto u_w_vpMatrix = glGetUniformLocation(waterProgram, "vpMatrix");
	auto u_w_reflectTex = glGetUniformLocation(waterProgram, "reflectTex");
	auto u_w_refractTex = glGetUniformLocation(waterProgram, "refractTex");
	auto u_w_dudvMap = glGetUniformLocation(waterProgram, "dudvMap");
	auto u_w_moveFactor = glGetUniformLocation(waterProgram, "moveFactor");
	auto u_w_cameraPos = glGetUniformLocation(waterProgram, "cameraPos");
	auto u_w_normalMap = glGetUniformLocation(waterProgram, "normalMap");
	auto u_w_lightPos = glGetUniformLocation(waterProgram, "lightPos");
	auto u_w_lightColor = glGetUniformLocation(waterProgram, "lightColor");
	auto u_w_depthMap = glGetUniformLocation(waterProgram, "depthMap");
	auto u_w_waveSpeed = glGetUniformLocation(waterProgram, "waveSpeed");
	auto u_w_waveMode = glGetUniformLocation(waterProgram, "waveMode");

	glUseProgram(waterProgram);

	glUniform1i(u_w_reflectTex, 1);
	glUniform1i(u_w_refractTex, 2);

	glUniform1i(u_w_dudvMap, 3);
	glUniform1i(u_w_normalMap, 5);
	glUniform1i(u_w_depthMap, 6);

	/*
	0 = flat
	1 = 2D sine wave
	2 = ripple wave?
	*/
	int waveMode = 2;
	glUniform1i(u_w_waveMode, waveMode);

	//============================================================================

	GLuint vboWater;	
	glGenBuffers(1, &vboWater);
	glBindBuffer(GL_ARRAY_BUFFER, vboWater);

	// auto waterPlane = genPlane(glm::vec3{0.0, 0.0, 1.5}, 
	// 						  glm::vec3{1.5, 0.0, 0.0}, 
	// 						  glm::vec3{-0.75, 0.0, -0.75}, 
	// 						  1);

	auto waterPlane = genPlane(glm::vec3{1.5, 0.0, 0.0}, 
							  glm::vec3{0.0, 0.0, -1.5}, 
							  glm::vec3{-0.75, 0.0, 0.75}, 
							  64);

	vector<VPosTexNormalTangent> water(waterPlane.size());

	for(size_t i = 0; i < waterPlane.size(); i++) {
		water[i].pos = waterPlane[i].pos;
		water[i].normal = waterPlane[i].normal;
		water[i].tex.x = (water[i].pos.x + 0.75)/1.5;
		water[i].tex.y = (water[i].pos.z - 0.75)/(-1.5);
		if(waveMode == 1)
			water[i].pos.y += sin(water[i].pos.x * 10)/20;
		else if(waveMode == 2)
			water[i].pos.y += 0.075 * cos(sqrt((water[i].pos.x*water[i].pos.x + water[i].pos.z*water[i].pos.z)*10));
	}

	glBufferData(GL_ARRAY_BUFFER, sizeof(VPosTexNormalTangent) * water.size(), water.data(), GL_STATIC_DRAW);

	GLuint vaoWater = 0;
	glGenVertexArrays(1, &vaoWater);
	glBindVertexArray(vaoWater);
	glBindBuffer(GL_ARRAY_BUFFER, vboWater);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VPosTexNormalTangent), (void*)(offsetof(VPosTexNormalTangent, tex)));

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//===============================REFLECTION======================================

	string vshReflection = "Water/reflection.vsh";
	string fshReflection = "Water/reflection.fsh";

	auto reflectionProgram = loadProgram(vshReflection, fshReflection);

	auto u_r_tex= glGetUniformLocation(reflectionProgram, "tex");
	
	glUseProgram(reflectionProgram);

	glUniform1i(u_r_tex, 1);

	//============================================================================

	GLuint vboReflection;
	glGenBuffers(1, &vboReflection);
	glBindBuffer(GL_ARRAY_BUFFER, vboReflection);
	
	GLfloat Reflection[] = {
		-1, 1, 0,		0, 1,
		-1, 0.5, 0,		0, 0,
		-0.5, 1, 0,		1, 1,
		-0.5, 0.5, 0,	1, 0
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(Reflection), Reflection, GL_STATIC_DRAW);

	GLuint vaoReflection = 0;
	glGenVertexArrays(1, &vaoReflection);
	glBindVertexArray(vaoReflection);
	glBindBuffer(GL_ARRAY_BUFFER, vboReflection);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (void*)(sizeof(GLfloat) * 3));

	//===============================REFRACTION=====================================

	string vshRefraction = "Water/Refraction.vsh";
	string fshRefraction = "Water/Refraction.fsh";

	auto refractionProgram = loadProgram(vshRefraction, fshRefraction);

	auto u_r2_tex= glGetUniformLocation(refractionProgram, "tex");
	
	glUseProgram(refractionProgram);

	glUniform1i(u_r2_tex, 2);

	//============================================================================

	GLuint vboRefraction;
	glGenBuffers(1, &vboRefraction);
	glBindBuffer(GL_ARRAY_BUFFER, vboRefraction);
	
	GLfloat Refraction[] = {
		0.5, 1, 0,		0, 1,
		0.5, 0.5, 0,	0, 0,
		1, 1, 0,		1, 1,
		1, 0.5, 0,		1, 0
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(Refraction), Refraction, GL_STATIC_DRAW);

	GLuint vaoRefraction = 0;
	glGenVertexArrays(1, &vaoRefraction);
	glBindVertexArray(vaoRefraction);
	glBindBuffer(GL_ARRAY_BUFFER, vboRefraction);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (void*)(sizeof(GLfloat) * 3));

	//============================================================================

	auto eye_pos = glm::vec3{1.0, 1.0, 1.0};
	auto light_pos = glm::vec3{0.0, 1.0, 0.0};
	auto light_color = glm::vec3{1.0, 1.0, 1.0};
	double xpos, ypos;

	float moveFactor = 0;
	float waveSpeed = 0;

	float indexCounter = 0;

	while (!glfwWindowShouldClose(window)) {

		glEnable(GL_CULL_FACE);
		glEnable(GL_CLIP_DISTANCE0);

		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT);

		{
			glfwGetCursorPos(window, &xpos, &ypos);
			ypos = (ypos / HEIGHT) * M_PI - M_PI * 0.5;
			xpos = (xpos / WIDTH) * 2 * M_PI - M_PI;
			
			eye_pos = glm::vec3{cos(ypos) * sin(xpos), 
								sin(ypos), 
								cos(ypos) * cos(xpos)} * 3.0f;
		}

		auto viewMatrix = glm::lookAt(eye_pos, glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
		auto projMatrix = glm::perspective(glm::radians(45.f), 1.0f, 0.1f, 100.f);;
		auto vpMatrix = projMatrix * viewMatrix;

		//==================================REFLECTION=================================

		{
			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, reflectionFbo);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			// glEnable(GL_DEPTH_TEST);

			// move the cam under the water
			float eyeDist;

			{ // draw the scene

				glUseProgram(cubemapProgram);
				glUniformMatrix4fv(u_c_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);				
				
				glBindVertexArray(vaoCubemap);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glUseProgram(poolProgram);

				eyeDist = 2 * eye_pos.y;
				eye_pos.y -= eyeDist;
				viewMatrix = glm::lookAt(eye_pos, glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
				vpMatrix = projMatrix * viewMatrix;
				
				glUniformMatrix4fv(u_p_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);
				clipPlane = glm::vec4(0.f, 1.f, 0.f, 0.f);
				glUniform4fv(u_p_plane, 1, glm::value_ptr(clipPlane));
				
				glBindVertexArray(vaoPool);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*2/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*3/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*4/5, pool.size()/5);
						
			}

			// move the cam back above the water
			eye_pos.y += eyeDist;
			viewMatrix = glm::lookAt(eye_pos, glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
			vpMatrix = projMatrix * viewMatrix;

			// unbind current framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(reflectionProgram);
			glBindVertexArray(vaoReflection);
			
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, reflectionTex);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		}

		//==================================REFRACTION=================================

		{
			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, refractionFbo);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			// glEnable(GL_DEPTH_TEST);

			{ // draw the scene

				glUseProgram(cubemapProgram);

				glUniformMatrix4fv(u_c_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);
				
				glBindVertexArray(vaoCubemap);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glUseProgram(poolProgram);
				
				glUniformMatrix4fv(u_p_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);
				clipPlane = glm::vec4(0.f, -1.f, 0.f, 0.f);
				glUniform4fv(u_p_plane, 1, glm::value_ptr(clipPlane));
				
				glBindVertexArray(vaoPool);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*2/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*3/5, pool.size()/5);
				glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*4/5, pool.size()/5);
						
			}

			// unbind current framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(refractionProgram);
			glBindVertexArray(vaoRefraction);
			
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, refractionTex);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		}

		//===============================REAL SCENE===============================

		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(cubemapProgram);

			glUniformMatrix4fv(u_c_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);
			
			glBindVertexArray(vaoCubemap);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

			glDisable(GL_CLIP_DISTANCE0);
			clipPlane = glm::vec4(0.f, 1.f, 0.f, 10000.f);

			glUseProgram(poolProgram);
			glBindVertexArray(vaoPool);

			glUniformMatrix4fv(u_p_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);		
			glUniform4fv(u_p_plane, 1, glm::value_ptr(clipPlane));
			
			glDrawArrays(GL_TRIANGLE_STRIP, 0, pool.size()/5);
			glDrawArrays(GL_TRIANGLE_STRIP, pool.size()/5, pool.size()/5);
			glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*2/5, pool.size()/5);
			glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*3/5, pool.size()/5);
			glDrawArrays(GL_TRIANGLE_STRIP, pool.size()*4/5, pool.size()/5);

		}

		{
			glUseProgram(floorProgram);
			glBindVertexArray(vaoFloor);

			indexCounter += 0.25;
			int index = floor(indexCounter);
			int xOffset = index % 4;
			int yOffset = floor(index/4);

			causticsOffset = glm::vec2{xOffset, yOffset};

			if(indexCounter >= 16)
				indexCounter = 0;

			glUniform2fv(u_f_causticsOffset, 1, glm::value_ptr(causticsOffset));		

			glUniformMatrix4fv(u_f_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);
			glUniform4fv(u_f_plane, 1, glm::value_ptr(clipPlane));

			glDisable(GL_CULL_FACE);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, floor2.size());

			glEnable(GL_CULL_FACE);
		}

		//=================================WATER==================================

		{
			// glDisable(GL_CULL_FACE);

			moveFactor += 0.003;
			moveFactor /= 1;

			waveSpeed += 0.01;

			glUseProgram(waterProgram);
			glBindVertexArray(vaoWater);

			glUniform3fv(u_w_cameraPos, 1, glm::value_ptr(eye_pos));
			glUniform3fv(u_w_lightPos, 1, glm::value_ptr(light_pos));
			glUniform3fv(u_w_lightColor, 1, glm::value_ptr(light_color));
			glUniform1f(u_w_moveFactor, moveFactor);
			glUniform1f(u_w_waveSpeed, waveSpeed);
			glUniformMatrix4fv(u_w_vpMatrix, 1, GL_FALSE, (float*)&vpMatrix);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, water.size());

		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
		
	glfwTerminate();
	return 0;
}
