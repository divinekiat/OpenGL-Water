#version 330
in layout(location = 0) vec3 pos;

out vec3 o_pos;

void main() {
	gl_Position = vec4(pos, 1.0);
	o_pos = pos;
}