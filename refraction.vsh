#version 330
in layout(location = 0) vec3 pos;
in layout(location = 1) vec2 texCoord;

out vec2 o_texCoord;

void main() {
	gl_Position = vec4(pos, 1.0);
	o_texCoord = texCoord;
}