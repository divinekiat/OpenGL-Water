#version 330

in vec2 o_texCoord;

uniform sampler2D tex;

out vec4 dest_color;

void main() {
	dest_color = mix(texture(tex, o_texCoord), vec4(0.0, 0.75, 1.0, 1.0), 0.5);
	// dest_color = vec4(1.0f);
}