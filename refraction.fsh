#version 330
in vec2 o_texCoord;

uniform sampler2D tex;

out vec4 dest_color;

void main() {
	dest_color = texture(tex, o_texCoord);

}