#version 330

in layout(location = 0) vec3 pos;
in layout(location = 1) vec2 texCoord;

uniform mat4 vpMatrix;
uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float waveSpeed;
uniform int waveMode;

out vec4 o_clipSpace;
out vec2 o_texCoord;
out vec3 o_toCameraVector;
out vec3 o_fromLightVector;

void main() {

	vec3 pos2 = pos;

	if(waveMode == 1)
		pos2.y = sin((pos2.x + waveSpeed)* 10)/20;
	else if(waveMode == 2) {
		float ws = waveSpeed + 1;
		pos2.y += 0.075 * cos(sqrt(((pos2.x + ws)*(pos2.x + ws) + (pos2.z + ws)*(pos2.z + ws))*10));
	}
	
	vec4 worldPos = vec4(pos2, 1.0);

	o_clipSpace = vpMatrix * worldPos;
	gl_Position = o_clipSpace;

	o_texCoord = texCoord / 2;
	o_toCameraVector = cameraPos - worldPos.xyz;
	o_fromLightVector = worldPos.xyz - lightPos;
} 